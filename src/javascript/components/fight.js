import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

function getRandomNumber(min, max) {
  return min + Math.random() * (max - min);
}

function getDamage(attacker, defender) {
  // return damage
  return defender >= attacker ? 0 : attacker - defender;
}

function getHitPower(attack) {
  // return hit power
  const criticalHitChance = getRandomNumber(1, 2);
  return criticalHitChance * attack;
}

function getBlockPower(defense) {
  // return block power
  const dodgeChance = getRandomNumber(1, 2);
  return dodgeChance * defense;
}

function getCriticalHit(attack) {
  // return critical hit power
  const criticalHit = attack * 2;
  return criticalHit;
}

function addCriticalTextBlock() {
  const critical = createElement({ tagName: 'div', className: 'arena___critical-block' });
  critical.innerText = 'CRITICAL';
  document.querySelector('.arena___root').append(critical);
  setTimeout(() => {
    critical?.remove();
  }, 2100);
}

function addPunchBackground(element, className) {
  setTimeout(() => {
    element.classList.toggle(className);
  }, 1200);
  setTimeout(() => {
    element.classList.toggle(className);
  }, 2100);
}

function animateAttack(element, className) {
  element.classList.toggle(className);
  setTimeout(() => {
    element.classList.toggle(className);
  }, 2100);
}

function setHealth(fighter, damage, el, value) {
  fighter.health = fighter.health - damage;
  el.style.width = `${value}%`;
}

function getHealthValue(damageValue, healthValue) {
  healthValue = healthValue - (damageValue / healthValue) * 100;
  return healthValue;
}

// resolve winner
function resolveWinner(resolve, firstFighter, secondFighter) {
  if (firstFighter.health <= 0) {
    resolve(secondFighter);
  } else if (secondFighter.health <= 0) {
    resolve(firstFighter);
  }
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const pressedKeys = new Set();
    const leftFighter = document.querySelector('.arena___left-fighter');
    const rightFighter = document.querySelector('.arena___right-fighter');
    const leftHealth = document.getElementById('left-fighter-indicator');
    const rightHealth = document.getElementById('right-fighter-indicator');
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;
    let damageToLeftFighter = 0;
    let damageToRightFighter = 0;
    let leftHealthValue = 100;
    let rightHealthValue = 100;
    let interval = 10;

    function hit(firstFighter, secondFighter) {
      if (pressedKeys.has(PlayerOneAttack) && !pressedKeys.has(PlayerOneBlock)) {
        const damage = getDamage(getHitPower(firstFighter.attack), getBlockPower(secondFighter.defense));
        const healthValue = getHealthValue(damage, rightHealthValue);
        damageToRightFighter = damage;
        rightHealthValue = healthValue;
        animateAttack(leftFighter, 'animate');
      } else if (pressedKeys.has(PlayerTwoAttack) && !pressedKeys.has(PlayerTwoBlock)) {
        const damage = getDamage(getHitPower(secondFighter.attack), getBlockPower(firstFighter.defense));
        const healthValue = getHealthValue(damage, leftHealthValue);
        damageToLeftFighter = damage;
        leftHealthValue = healthValue;
        animateAttack(rightFighter, 'animate-right');
      }
    }

    function controlHealth(firstFighter, secondFighter) {
      if (pressedKeys.has(PlayerTwoAttack) && !pressedKeys.has(PlayerOneBlock)) {
        setHealth(firstFighter, damageToLeftFighter, leftHealth, leftHealthValue);
        addPunchBackground(leftFighter, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      } else if (pressedKeys.has(PlayerOneAttack) && !pressedKeys.has(PlayerTwoBlock)) {
        setHealth(secondFighter, damageToRightFighter, rightHealth, rightHealthValue);
        addPunchBackground(rightFighter, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      }
    }

    function criticalHit(firstFighter, secondFighter, code) {
      if (PlayerOneCriticalHitCombination.includes(code)) {
        const damage = getCriticalHit(firstFighter.attack);
        const healthValue= getHealthValue(
          damage,
          rightHealthValue
        );
        damageToRightFighter = damage;
        rightHealthValue = healthValue;
        setHealth(secondFighter, damageToRightFighter, rightHealth, rightHealthValue);
        animateAttack(leftFighter, 'animate');
        addCriticalTextBlock();
        addPunchBackground(rightFighter, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      } else if (PlayerTwoCriticalHitCombination.includes(code)) {
        const damage = getCriticalHit(secondFighter.attack);
        const healthValue = getHealthValue(
          damage,
          leftHealthValue
        );
        damageToLeftFighter = damage;
        leftHealthValue = healthValue;
        setHealth(firstFighter, damageToLeftFighter, leftHealth, leftHealthValue);
        animateAttack(rightFighter, 'animate-right');
        addCriticalTextBlock();
        addPunchBackground(leftFighter, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      }
    }

    function handlePressedKeysEvent(e) {
      pressedKeys.add(e.code);
    }

    function handleUnpressedKeysEvent(e) {
      hit(firstFighter, secondFighter);
      controlHealth(firstFighter, secondFighter);
      if (interval === 10 && pressedKeys.size === 3) {
        criticalHit(firstFighter, secondFighter, e.code);
        let timer = setInterval(() => {
          interval--;
          if (interval === 0) {
            clearInterval(timer);
            interval = 10;
          }
        }, 1000);
      }
      pressedKeys.delete(e.code);
    }

    document.addEventListener('keydown', handlePressedKeysEvent);
    document.addEventListener('keyup', handleUnpressedKeysEvent);
  });
}
