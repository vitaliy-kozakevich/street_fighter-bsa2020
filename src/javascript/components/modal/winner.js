import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  const bodyElement = createElement({ tagName: 'div', className: 'arena-modal___over' });
  const fighterImage = createFighterImage(fighter);
  bodyElement.append(fighterImage);
  showModal({
    title: `${fighter.name} Wins`,
    bodyElement,
    onClose: () => {
      document.location.reload(true);
    },
  });
}
